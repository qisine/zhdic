Zhdic::Application.routes.draw do
  resources :dict_entries, only: [:index, :show]
  match 'e/:term' => 'dict_entries#search', exact: true
  match 's/:term' => 'dict_entries#search', exact: false
  match 'search/:term' => 'dict_entries#search', exact: false

  root to: "dict_entries#index"
end
