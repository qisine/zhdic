require 'active_record'

ActiveRecord::Base.establish_connection(
  adapter: 'postgresql',
  host: 'localhost',
  username: 'zhdic',
  password: 'zhdic',
  database: 'zhdic_db',
  port: 5432,
  encoding: 'unicode',
  pool: 5
)
