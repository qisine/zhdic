class Object
  def method_missing(m, *args, &block)
    return DictEntry.where(hanzi: m).first if Character.hanzi? m
    return Translation.where(pinyin: m).to_a if Character.pinyin? m 
    super
  end
end
