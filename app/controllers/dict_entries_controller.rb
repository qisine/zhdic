class DictEntriesController < ApplicationController
  def index
  end

  def search
    term = params["exact"] ? params["term"] : "%#{params['term']}%"
    term = term.gsub(/\*+/, "%")

#placeholders = term.gsub(/[^_*]+/, "?").gsub(/\*+/,"%")
#    strings = term.split(/[_*]+/).reject {|e| e.empty?}

    @des = DictEntry.where("hanzi like ?", term)
  end
    
  def show
    @de = DictEntry.find(params[:id])
  end
end
